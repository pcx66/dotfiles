# For 'emacsclinet -t' to work
export ALTERNATE_EDITOR='emacs --server'
export EDITOR='emacsclient -t'
export VISUAL='emacscleint -t'
#For virtualenvwrapper
source /usr/local/bin/virtualenvwrapper_lazy.sh

# Wraps a completion function
# make-completion-wrapper <actual completion function> <name of new func.>
#                         <command name> <list supplied arguments>
# eg.
#   alias agi='apt-get install'
#   make-completion-wrapper _apt_get _apt_get_install apt-get install
# defines a function called _apt_get_install (that's $2) that will complete
# the 'agi' alias. (complete -F _apt_get_install agi)
#
function make-completion-wrapper () {
    local function_name="$2"
	local arg_count=$(($#-3))
	    local comp_function_name="$1"
		shift 2
		    local function="
function $function_name {
    ((COMP_CWORD+=$arg_count))
    COMP_WORDS=( "$@" \${COMP_WORDS[@]:1} )
    "$comp_function_name"
    return 0
}"
			eval "$function"
}


# aliases added to ~/.bash_aliases file

# enabling completions to some aliases specifically
# we create a _git_checkout_mine function that will do the completion for "gco"
# using the completion function "_git"
make-completion-wrapper _git _git_checkout_mine git checkout
# similarly for other aliases that need completions
make-completion-wrapper _git _git_mine git
make-completion-wrapper _git _git_status_mine git status
make-completion-wrapper _git _git_diff_mine git diff
make-completion-wrapper _git _git_add_mine git add
make-completion-wrapper _git _git_merge_mine git merge
make-completion-wrapper _git _git_commit_mine git commit
make-completion-wrapper _git _git_log_mine git log
make-completion-wrapper _git _git_shortlog_mine git log --oneline --graph --decorate
make-completion-wrapper _git _git_push_mine git push
make-completion-wrapper _git _git_pull_mine git pull
make-completion-wrapper _git _git_gplod_mine git pull origin development
make-completion-wrapper _git _git_gplom_mine git pull origin master
make-completion-wrapper _git _git_remote_mine git remote
make-completion-wrapper _git _git_ra_mine git remote add
make-completion-wrapper _git _git_rr_mine git remote rm
make-completion-wrapper _git _git_clone_mine git clone
make-completion-wrapper _git _git_branch_mine git branch
make-completion-wrapper _git _git_stash_mine git stash
make-completion-wrapper _git _git_gspop_mine git stash pop

# we tell bash to actually use _git_checkout_mine to complete "gco"
complete -o bashdefault -o default -o nospace -F _git_checkout_mine gco
complete -o bashdefault -o default -o nospace -F _git_mine g
complete -o bashdefault -o default -o nospace -F _git_status_mine s
complete -o bashdefault -o default -o nospace -F _git_diff_mine d
complete -o bashdefault -o default -o nospace -F _git_add_mine a
complete -o bashdefault -o default -o nospace -F _git_merge_mine m
complete -o bashdefault -o default -o nospace -F _git_commit_mine gc
complete -o bashdefault -o default -o nospace -F _git_log_mine gl
complete -o bashdefault -o default -o nospace -F _git_shortlog_mine gls
complete -o bashdefault -o default -o nospace -F _git_push_mine gp
complete -o bashdefault -o default -o nospace -F _git_pull_mine gpl
complete -o bashdefault -o default -o nospace -F _git_gplom_mine gplom
complete -o bashdefault -o default -o nospace -F _git_gplod_mine gplod
complete -o bashdefault -o default -o nospace -F _git_remote_mine gr
complete -o bashdefault -o default -o nospace -F _git_ra_mine gra
complete -o bashdefault -o default -o nospace -F _git_rr_mine grr
complete -o bashdefault -o default -o nospace -F _git_clone_mine gcl
complete -o bashdefault -o default -o nospace -F _git_branch_mine gb
complete -o bashdefault -o default -o nospace -F _git_stash_mine gst
complete -o bashdefault -o default -o nospace -F _git_gspop_mine gspop
