#!/bin/bash
rsync ~/.bash_aliases .
rsync ~/.bash_profile .
rsync -r ~/.config/awesome .config/.
rsync ~/.config/redshift.conf .config/.
rsync -r ~/.emacs.d/*.el .emacs.d/.
rsync ~/.gitconfig .
rsync ~/.xmodmap .
rsync /etc/X11/xorg.conf .
