;;; 3rdparty_config ---  3rd party packages specific config
;;; commentary:

;;; code:

;; load powerline
(require 'powerline)
(powerline-default-theme)

;; Enable bbatsov/guru-mode
(require 'guru-mode)
(guru-global-mode +1)
 
;; enable auto-complete
(require 'auto-complete-config)
(ac-config-default)

;; disable line numbers for shell, etc buffers
(require 'linum-off-autoloads)

;; enable yasnippet
(require 'yasnippet)
(yas/global-mode 1)

;; Setup Jedi.el
(setq jedi:setup-keys t)
(require 'jedi)
(add-hook 'python-mode-hook 'jedi:setup)

;; Enable flycheck globally
(add-hook 'after-init-hook #'global-flycheck-mode)

;; Enable autopair globally
(autopair-global-mode t)

;; Use flake8 for flymake stuff
(setq flymake-python-pyflakes-executable "flake8")

;; Web templates support using web-mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(defun web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2))
(add-hook 'web-mode-hook 'web-mode-hook)
(setq web-mode-markup-indent-offset 2)
(setq web-mode-css-indent-offset 2)
(setq web-mode-code-indent-offset 2)
(setq web-mode-comment-style 2)
(setq web-mode-enable-block-faces t)


;; ace-jump-mode
(require 'ace-jump-mode)
(global-set-key (kbd "C-c SPC") 'ace-jump-word-mode)

;; support for mark-more-like-this
(require 'mark-more-like-this)
(global-set-key (kbd "C-<") 'mark-previous-like-this)
(global-set-key (kbd "C->") 'mark-next-like-this)
(global-set-key (kbd "C-M-m") 'mark-more-like-this)
(global-set-key (kbd "C-*") 'mark-all-like-this)

;; bindings for expand-region
(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

;; bindings for magit mode
(global-set-key (kbd "C-x m")  'magit-status)

(provide '3rdparty_config)
;;; 3rdparty_config ends here
