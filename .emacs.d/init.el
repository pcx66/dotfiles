;;; pcx66 --- emacs config
;;; Commentary:
;;; Code:

(add-to-list 'load-path "~/.emacs.d")

;; intialize 'package', and add repositories
(package-initialize)
(setq package-archives
      (quote (("marmalade" . "http://marmalade-repo.org/packages/")
	      ("gnu" . "http://elpa.gnu.org/packages/")
	      ("melpa" . "http://melpa.milkbox.net/packages/"))))

;; setting font-height to 12pt
(set-face-attribute 'default nil :height 120)

;; load 'zenburn' theme
;; (when (display-graphic-p)
;;  (load-theme 'zenburn t))
(load-theme 'zenburn t)

;; enables pending-delete-mode
(pending-delete-mode t)

;; Enabling line numbers globally by default
(global-linum-mode 1)

;; enables X11 Clipboard support
(setq x-select-enable-clipboard t)

;; highlight current line, globally
(global-hl-line-mode 0)

;; Adds a suffix to the buffer names. The suffix is the directory
;; name of the buffer, necessary for django dev
(require 'uniquify)
(setq uniquify-buffer-name-style 'reverse)
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t)

;; This sets the variable c-default-style such that
;; all cc-mode uses linux style except for java & awk
(setq c-default-style '((java-mode . "java")
			(awk-mode . "awk")
                        (other . "linux")))

;; Enabling Ido Mode
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

;; enable visual feedback on selections
(setq transient-mark-mode t)

;; helps complete commands in mini-buffer
(icomplete-mode 1)

;; shows the column number in which the pointer is, helps for pep-8 style
(column-number-mode 1)

;; show paren mode
(show-paren-mode 1)

;; disable menubar, toolbar, scrollbar
(menu-bar-mode 0)
(tool-bar-mode -1)
(scroll-bar-mode 0)

;; change html-mode indent level to 4
(add-hook 'html-mode-hook
	  (lambda()
	    (set (make-local-variable 'sgml-basic-offset) 4)))

;; Changing C-w functionality to that of 'bash'
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)

;; re-map kill-whole-line
(global-set-key (kbd "M-k") 'kill-whole-line)

;; moves to the window on the left
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>")  'windmove-right)
(global-set-key (kbd "C-c <up>")  'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

;; switching buffers
(global-set-key (kbd "C-x p")  'previous-buffer)
(global-set-key (kbd "C-x n")  'next-buffer)

;; rever buffer keybinding
(global-set-key (kbd "C-c r") 'revert-buffer)

;; key-bindings to kill emacs when using daemon mode
(global-set-key (kbd "C-c C-x") 'save-buffers-kill-emacs)

;; disable mouse events
(dolist (k '([mouse-1] [down-mouse-1] [drag-mouse-1] [double-mouse-1] [triple-mouse-1]
             [mouse-2] [down-mouse-2] [drag-mouse-2] [double-mouse-2] [triple-mouse-2]
             [mouse-3] [down-mouse-3] [drag-mouse-3] [double-mouse-3] [triple-mouse-3]
             [mouse-4] [down-mouse-4] [drag-mouse-4] [double-mouse-4] [triple-mouse-4]
             [mouse-5] [down-mouse-5] [drag-mouse-5] [double-mouse-5] [triple-mouse-5]))
  (global-unset-key k))

(load "rename_sgml_tag")
(add-hook 'sgml-mode-hook
	  (lambda ()
	    (require 'rename-sgml-tag)
	    (define-key sgml-mode-map (kbd "C-c C-r") 'rename-sgml-tag)))

(load "3rdparty_config")
(load "customize_config")

(provide 'init)
;;; init.el ends here
