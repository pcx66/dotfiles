(package-initialize)
(setq package-archives 
      (quote (("marmalade" . "http://marmalade-repo.org/packages/") 
	      ("gnu" . "http://elpa.gnu.org/packages/")
	      ("melpa" . "http://melpa.milkbox.net/packages/"))))

(defvar my-packages '(
		      auto-complete
		      auto-indent-mode
		      autopair
		      dired+
		      flycheck
		      flymake
		      flymake-python-pyflakes
		      ipython
		      javadoc-help
		      jedi
		      js2-mode
		      json
		      less-css-mode
		      linum-off
		      magit
		      markdown-mode
		      mark-multiple
		      mark-more-like-this
		      org
		      pep8
		      pony-mode
		      pylint
		      pymacs
		      sass-mode
		      scss-mode
		      shell-here
		      tabbar
		      tabbar-ruler
		      virtualenv
		      wget
		      zenburn-theme
		      ))

(dolist (p my-packages) 
  (when (not (package-installed-p p))
    (package-install p)))
